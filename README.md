**ขั้นตอนการติดตั้ง และเตียมเครื่องมือ**
1. ติดตั้ง Git (สำหรับคนที่ยังไม่มี) ตรวจสอบโดยใช้คำสั่ง `git version` `ผลที่ได้ git version 2.32.1 (Apple Git-133)` เข้าไปที่เว็บ https://git-scm.com/downloads เลือก load ตามระบบปฎิบัตการ
2. ติดตั้ง node.js (https://nodejs.org/en/) (ตรวจสอบโดยใช้คำสั่ง `node -v`)
3. ติดตั้ง Angular https://angular.io/cli `npm install -g @angular/cli` หลังจาก ติดตั้งตรวจสอบด้วยคำสั่ง `ng version`
4. download source จาก git URL https://gitlab.com/suradet/wunca41 เลือก Branches wunca41

**First Angular**
1. เข้าไปที่ folder Wunca41AngularAPI `cd Wunca41AngularAPI`
2. รันคำสั่ง `npm install`
3. ทดสอบรัน `ng serve`
4. เข้าเว็บ http://localhost:4200
